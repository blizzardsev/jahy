# About jahy

Jahy is a simple Python-based serverless function for pinging Twitter for updates on a specific account and delivering tweets to Discord via webhook.

It will periodically check the designated Twitter account (as found via supplied ID), approximately every hour. A small collection of last tweets made is retrieved and the most recent checked against a record containing the ID of the last most recent tweet made. If they differ, a new tweet has been made - this is then pushed to the webhook.

## CI/deployment script configuration

### Dependencies

* **AWS CLI**: Command-line toolkit for AWS @ https://aws.amazon.com/cli/
* **AWS SAM CLI**: Command-line toolkit for AWS SAM @ https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/serverless-sam-cli-install.html
* **pigar**: Automatic Python requirements.txt @ https://github.com/damnever/pigar

## Python configuration

### Runtime

* Python 3.8

### Dependencies

* Boto3: AWS toolkit @ https://boto3.amazonaws.com/v1/documentation/api/latest/guide/quickstart.html
* Requests: HTTP for Humans @ https://requests.readthedocs.io/en/master/

### Environment variables

* **ALERT_TOPIC_ARN** : The ARN of the alert topic to send notifications to in the event of controlled failure.
* **BUCKET_NAME** : The name of the bucket (**not** ARN/URL) containing the last_tweet_id.txt file, under key `lambda/jahy/last_tweet_id.txt`.
* **DEPLOY_STAGE** : The name of the current deployment stage; this determines what stage (dev/release) to pull metrics from for each targeted item.
* **DISPLAY_AVATAR_URL** : The URL specifying the profile picture to display for the sender of the messages on Discord.
* **DISPLAY_USERNAME** : The username to display as the sender of the messages on Discord.
* **NOTIFY_TOPIC_ARN** : The ARN of the Notipy submission topic for Notipy solution integration.
* **TAG_ROLE_ID** : The ID of the role to tag on Discord
* **TWITTER_USER_ID** : The ID of the Twitter account to probe for Tweets to return to the webhook
* **WEBHOOK_URL** : The URL identifying the channel on Discord to post to.

## Deployment

Use script *deploy_to_dev_stack.bat* *version-number* *webhook-url* *bearer-token* for manual deployments; E.G `deploy_to_dev_stack.bat 1.0.0 www.mysite.com ABC123`. Execute within jahy directory.
Release deployments are handled by CI.

## Contact

*blizzardsev.dev@gmail.com*