from datetime import datetime
import json
import logging
import os
import sys
import traceback
sys.path.insert(0, 'packages')
import boto3
import requests

__BUCKET_NAME = os.environ['BUCKET_NAME']

def lambda_handler(event, context):
    """
    Lambda entry point.
    """
    try:
        # Get the stored ID of the last tweet posted
        global __BUCKET_NAME
        last_tweet_id = boto3.resource('s3').Object(
            __BUCKET_NAME,
            'lambda/jahy/last_tweet_id.txt').get()['Body'].read().decode('utf-8')

        # Get the tweet data for the account
        twitter_response = requests.get(
                f'https://api.twitter.com/2/users/{os.environ["TWITTER_USER_ID"]}/tweets',
                headers={
                    'Authorization': f'Bearer {os.environ["BEARER_TOKEN"]}'
                },
                params={
                    'max_results': 5,
                    'expansions': 'in_reply_to_user_id'
                })

        if twitter_response.status_code == 200:
            twitter_data = json.loads(twitter_response.content)
            if 'data' in twitter_data:
                for tweet in twitter_data['data']:
                    if tweet.get('in_reply_to_user_id'):
                        # Skip any tweets that are replies
                        logging.info(f'Tweet ID {tweet["id"]} is a reply, skipping...')
                        pass

                    elif tweet['id'] == last_tweet_id:
                        # This is the last tweek we acted on, stop here so we don't process tweets older than that
                        return f'Tweet ID {tweet["id"]} has already been delivered, ending.'

                    else:
                        # Newest recent tweet that isn't a reply to something; post to webhook to push announcement
                        discord_data = {
                            'username': os.environ['DISPLAY_USERNAME'],
                            'avatar_url': os.environ['DISPLAY_AVATAR_URL'],
                            'content': f'<@&{os.environ["TAG_ROLE_ID"]}> https://twitter.com/i/web/status/{tweet["id"]}',
                        }

                        # Post to webhook
                        headers = {'content-type': 'application/json'}
                        response = requests.post(
                            url=os.environ['WEBHOOK_URL'],
                            data=json.dumps(discord_data),
                            headers=headers)

                        # Update reference file on S3
                        boto3.resource('s3').Object(
                            __BUCKET_NAME,
                            'lambda/jahy/last_tweet_id.txt').put(Body=bytes(tweet['id'].encode('UTF-8')))

                        return "Recent tweet found and posted to webhook."

            else:
                logging.error(f'Unable to retrieve tweets; no tweets were found.')
                return

        else:
            logging.error(f'Unable to retrieve tweets; HTTP status code was: {twitter_response.status_code}')

    except:
        # Notify alert topic on failure and raise the exception
        logging.error('Failed to process tweets; an exception occurred.')
        logging.error(f'Exception info: {traceback.format_exc()}')
        boto3.client('sns').publish(TopicArn=os.environ['NOTIFY_TOPIC_ARN'],
            Subject='jahy failed: could not retrieve tweets or deliver message.',
            Message=traceback.format_exc(),
            MessageAttributes={
                'DiscordDisplayAllData': {
                    'DataType': 'String',
                    'StringValue': 'true'
                },
                'EventJson': {
                    'DataType': 'String',
                    'StringValue': json.dumps(event)
                }
            })